-- Adds default rails

local PART={}
PART.ID = "portal_scannertogglehitbox"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/controlhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)