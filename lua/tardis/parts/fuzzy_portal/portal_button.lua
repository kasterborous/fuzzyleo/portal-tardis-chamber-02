-- Adds default rails

local PART={}
PART.ID = "portal_button"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/button.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)