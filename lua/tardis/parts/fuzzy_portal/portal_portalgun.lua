-- Adds default rails

local PART={}
PART.ID = "portal_portalgun"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/portalgun.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)