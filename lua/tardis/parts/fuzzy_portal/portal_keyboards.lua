-- Adds default rails

local PART={}
PART.ID = "portal_keyboards"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/keyboards.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)