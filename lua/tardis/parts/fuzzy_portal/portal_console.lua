-- Adds default rails

local PART={}
PART.ID = "portal_console"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/console.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)