-- Adds default rails

local PART={}
PART.ID = "portal_stairstuff"
PART.Name = "stairs"
PART.Model = "models/FuzzyLeo/PortalTARDIS/stairstuff.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)