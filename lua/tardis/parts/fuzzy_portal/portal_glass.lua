-- Adds default rails

local PART={}
PART.ID = "portal_glass"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/glass.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)