-- Adds default rails

local PART={}
PART.ID = "portal_monitors"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/monitors.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)