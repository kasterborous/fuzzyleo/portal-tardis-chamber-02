-- Adds default rails

local PART={}
PART.ID = "portal_mainprops"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/MainProps.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)