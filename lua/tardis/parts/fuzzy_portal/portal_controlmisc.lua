-- Adds default rails

local PART={}
PART.ID = "portal_controlmisc"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/staticconsole.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)