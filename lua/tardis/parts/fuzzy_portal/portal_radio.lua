-- Adds default rails

local PART={}
PART.ID = "portal_radio"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/PortalTARDIS/radio.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)