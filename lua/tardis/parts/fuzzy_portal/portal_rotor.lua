-- Adds default rails

local PART={}
PART.ID = "portal_rotor"
PART.Name = "stairs"
PART.Model = "models/FuzzyLeo/PortalTARDIS/consolerotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)